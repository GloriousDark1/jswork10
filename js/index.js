"use strict"


const buttons = document.querySelectorAll('.tabs-title');
buttons.forEach((btn) => {
	btn.addEventListener('click', clickButton)
})

function clickButton(event) {
	let buttonAtr = this.getAttribute('data-name');
	let text = document.querySelector(`.tabs-content [data-name = ${buttonAtr}]`);
	document.querySelector('.seen').classList.remove('seen');
	text.classList.add('seen');
	document.querySelector('.tabs-title.active').classList.remove('active');
	this.classList.add('active');
}